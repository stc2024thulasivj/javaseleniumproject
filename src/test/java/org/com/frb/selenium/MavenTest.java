package org.com.frb.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.time.Duration;


public class MavenTest {
    WebDriver driver;

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--headless");
        opt.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        opt.addArguments("--no-sandbox"); // Bypass OS security model
        opt.addArguments("--remote-debugging-port=9222");
        opt.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(opt);
        driver.get("https://www.google.com/");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

    }

    @Test
    public void assertTitle() {
        String currentUrl = driver.getCurrentUrl();
        System.out.println("Navigate to website: " + currentUrl);
        String currentTitle = driver.getTitle();
        System.out.println("Displayed title as: " + currentTitle);
    }

    @After
    public void tearDown(){
        driver.quit();
    }

}
